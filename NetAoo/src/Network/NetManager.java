/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Network;

import java.io.File;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author angelocarraggi
 */
public class NetManager {
    private ServerSocket serve;
    private ArrayList workers;
    
    
    public NetManager() {
        workers = new ArrayList();
        try {
            serve = new ServerSocket(60000);
            while(true){
            Socket s = serve.accept();
            workerPeer peer = new workerPeer(s,this);
            workers.add(peer);
            peer.start();
            }
        } catch (IOException ex) {
            Logger.getLogger(NetManager.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
    synchronized public void removePeer(workerPeer p){
        try {
            Thread.sleep(3000);
        } catch (InterruptedException ex) {
            Logger.getLogger(NetManager.class.getName()).log(Level.SEVERE, null, ex);
        }
       int i=0;
        for (Object worker : workers) {
            if(worker.equals(p)){
               
                workers.remove(worker);
                 System.err.println("eliminato size:"+workers.size());
                break;
            }
       
        }
        notifyAll();
           
        
    }
    
    synchronized public File[] getListFiles(String directory){
        File f = new File(directory);
        System.out.println(f.isDirectory());
        return f.listFiles();
    }
    
    synchronized public File getFile(String path){
        File f = new File(path);
        return f;
    }
}
