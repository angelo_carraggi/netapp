/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Network;

import com.sun.corba.se.impl.orbutil.ObjectWriter;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author angelocarraggi
 */
public class workerPeer extends Thread{
    private Socket client;
    private NetManager manager;
    private PrintWriter pw;
    private  ObjectOutputStream oos;

    public workerPeer(Socket client,NetManager m) {
        this.client = client;
        this.manager = m;
    }
    
    /**
     * Metodo di avvio del thread
     * Esso deve tenere conto di cosa si sta per passare.
     */
    @Override
    public void run() {
        BufferedReader br = null;
        try {
            super.run(); //To change body of generated methods, choose Tools | Templates.
            String s = "";
            br = new BufferedReader(new InputStreamReader(client.getInputStream()));
            pw = new PrintWriter(client.getOutputStream());
            oos = new ObjectOutputStream(client.getOutputStream());
            while(!s.equals("exit")){
                s = br.readLine();
                checkAction(s);
                System.out.println(s);
                pw.write(s+"\n");
                pw.flush();
            
            }
            manager.removePeer(this);
        } catch (IOException ex) {
            Logger.getLogger(workerPeer.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                br.close();
            } catch (IOException ex) {
                Logger.getLogger(workerPeer.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
    }
    
    
    public void checkAction(String s){
        String[] strs = s.split(" ");
        if(strs[0].equals("listFiles")){ 
            try {
                System.out.println(strs[1]);
            File[] v = manager.getListFiles(strs[1]);
            System.out.println(v.length);
            oos.writeObject(v);
            oos.flush();
            } catch (IOException ex) {
                Logger.getLogger(workerPeer.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        System.err.println(strs[0]);
        if(strs[0].equals("getFile")){
            FileInputStream fis = null;
            try {
                File f = manager.getFile(strs[1]);
                fis = new FileInputStream(f);
               
                byte[] b;
                b = new byte[1];
                ArrayList bytefile = new ArrayList();
                while(fis.read(b)!=-1){
                   bytefile.add(b);
                    
                }
                oos.writeObject(bytefile);
                        } catch (FileNotFoundException ex) {
                Logger.getLogger(workerPeer.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(workerPeer.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                try {
                    fis.close();
                } catch (IOException ex) {
                    Logger.getLogger(workerPeer.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        
  
        
    }
    
}
