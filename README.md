# README #

Questo progetto riguarda la creazione di un'applicazione che permetta l'autoconfiguramento di una rete locale.
Viene utilizzato il linguaggio java.



### Cosa contiene il repository? ###

Questo repository contiene due cartelle principali, tra cui una da eliminare. 
Quella che si dovrà tenere conto è la cartella NetApp.
Esso è un progetto java generato da netbeans.


### Come settarlo? ###

Il setup di questo progetto è semplice, in quanto una volta effettuato il checkout del progetto,
basta un ide java per importare tutto il progetto con tutti i suoi settaggi.
Nel caso non fosse possibile, ricordo che la parte per quanto riguarda i test viene messa all'interno di un'apposita cartella test
in cui al suo interno vi è la classe con la main function implementata.

Per avviare invece la parte di progetto/applicazione effettiva, basta avviare la classe principale NetApp in cui viene implementata la main function

### Who do I talk to? ###

Per altre info contattemi in privato.