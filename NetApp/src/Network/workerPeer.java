/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Network;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.nio.file.Files;
import java.util.logging.Level;
import java.util.logging.Logger;
import sun.misc.BASE64Encoder;

/**
 *
 * @author angelocarraggi
 */
public class workerPeer extends Thread{
    private final Socket client;
    private final NetManager manager;
    private PrintWriter pw;
    private  ObjectOutputStream oos;

    public workerPeer(Socket client,NetManager m) {
        this.client = client;
        this.manager = m;
    }
    
    /**
     * Metodo di avvio del thread
     * Esso deve tenere conto di cosa si sta per passare.
     */
    @Override
    public void run() {
        BufferedReader br = null;
        try {
            super.run(); 
            String s = "";
            br = new BufferedReader(new InputStreamReader(client.getInputStream()));
            pw = new PrintWriter(client.getOutputStream());
            oos = new ObjectOutputStream(client.getOutputStream());
            while(!s.equals("exit")){
                s = br.readLine();
                checkAction(s);
               /// System.out.println(s);
                pw.write(s+"\n");
                pw.flush();
            
            }
            manager.removePeer(this);
        } catch (IOException ex) {
            Logger.getLogger(workerPeer.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                br.close();
            } catch (IOException ex) {
                Logger.getLogger(workerPeer.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
    }
    
    
    
    /**
     * Migliorata la parte di trasferimento dei file, resa funzionante.
     * E' un po' lentina in quanto utilzza stringhe non compresse. 
     * 
     * @param s 
     */
    public void checkAction(String s){
        String[] strs = s.split(" ");
        if(strs[0].equals("listFiles")){ 
            try {
            File[] v = manager.getListFiles(strs[1]);
            oos.writeObject(v);
            oos.flush();
            } catch (IOException ex) {
                Logger.getLogger(workerPeer.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if(strs[0].equals("getFile")){
            FileInputStream fis = null;
            try {
                File f = manager.getFile(strs[1]);
                fis = new FileInputStream(f);
                byte[] files = Files.readAllBytes(f.toPath());
                BASE64Encoder bs64 = new BASE64Encoder();
                
                String value = bs64.encode(files);
                OutputStream os = client.getOutputStream();
                pw.write(String.valueOf(files.length)+"\n");
                System.out.println(value.length());
                System.out.println(value);
                pw.flush();
                pw.write(value);
                pw.flush();
                pw.write("\nend\n");
                pw.flush();
                
                
                        } catch (FileNotFoundException ex) {
                Logger.getLogger(workerPeer.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(workerPeer.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                try {
                    fis.close();
                } catch (IOException ex) {
                    Logger.getLogger(workerPeer.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
         if(strs[0].equals("sendFile")){
             
         }
         
  
        
    }
    
}
