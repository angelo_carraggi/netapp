/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Network;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.nio.charset.StandardCharsets;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author AxC1011
 */
public class udpPeer extends Thread {

    private DatagramSocket server;
    private Connection manager;
    
    public udpPeer(Connection s) {
        try {
            server = new DatagramSocket(60001);
           manager = s;
            
        } catch (SocketException ex) {
            Logger.getLogger(udpPeer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(udpPeer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    
    @Override
    public void run() {
        try {
            super.run(); //To change body of generated methods, choose Tools | Templates.
            
      
            
            
            
            byte[] sendData = new byte[1024];
            
            
            
            sendData = manager.ip_address.getBytes();
            InetAddress addr = InetAddress.getByName("255.255.255.255");
            DatagramPacket DP = new DatagramPacket(sendData, sendData.length, addr, 60001);
            DatagramSocket ds = new DatagramSocket();
            ds.send(DP);
            DatagramPacket dp_server = new DatagramPacket( sendData, sendData.length);
            server.receive(dp_server);
            String str = new String(dp_server.getData(), StandardCharsets.UTF_8);
            if (str.equals(manager.ip_address)){
                System.out.println("da scartare questo ip");
            }
        } catch (IOException ex) {
            Logger.getLogger(udpPeer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
