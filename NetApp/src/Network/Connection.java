/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Network;

import java.io.IOException;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.SocketAddress;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author angelocarraggi
 */
public class Connection {
    public String ip_address;
    public int id;
    private ServerSocket serve;
    /*
    Questa classe deve permettere l'intelligenza della rete.
    Si compone di due step importanti quali:
        - inviare una richiesta a tutti
        - inviare la propria presenza a tutti.
    Inviare una richiesta a tutti significa che si vuole identificare il numero 
    che viene associato al socket in questione in modo da poterne garantire 
    una facile ricostruzione della rete.
    */

    public Connection() {
      
        try {
            ip_address = InetAddress.getLocalHost().getHostAddress();
            serve = new ServerSocket(60000);
            new udpPeer(this).start();
            
           
            
        } catch (IOException ex) {
            Logger.getLogger(Connection.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
}
